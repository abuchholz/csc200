#Just Adds Value until reaches 11 starts at 3
from itertools import count

for i in count(3):
  print(i)
  if i >=11:
    break


#Accumulate starts at 0 and increases to 1 then 2 and so on. Takewhile takes items from an iterable while a predicate function remains true;
from itertools import accumulate, takewhile

nums = list(accumulate(range(10)))
print(nums)
print(list(takewhile(lambda x: x<= 6, nums)))


#Permutations creates all the combinations with the letters. 
from itertools import product, permutations

letters = ("A", "B")
print(list(product(letters, range(3))))
print(list(permutations(letters))) 


#Example Take range and mutlply by length of the list. 
from itertools import product
a={1, 2}
print(len(list(product(range(5), a))))

