#Examples #1 - Basics of Sets
num_set = {1, 2, 3, 4, 5}
word_set = set(["spam", "eggs", "sausage"])

# The Number 3 is Present in Num_Set
print(3 in num_set)

# Although spam is present the not makes it False
print("spam" not in word_set)

##########################################################################################

#Example #2 - Problem #1 | Prints 1 because "e" is not present
letters = {"a", "b", "c", "d"}
if "e" not in letters:
  print(1)
else: 
  print(2)

##########################################################################################

#Example #3 - Adding and Remove Nums in Sets.
nums = {1, 2, 1, 3, 1, 4, 5, 6}
print(nums)
nums.add(-7) #Add instead of Append like in Lists
nums.remove(3)
print(nums)

#len used to determine length of set (like list), add to add item to set, and remove to remove an item from a set.

##########################################################################################

#Example 4 - 
#Sets can be combined using mathematical operations.
#The union operator | combines two sets to form a new one containing items in either.
#The intersection operator & gets items only in both.
#The difference operator - gets items in the first set but not in the second.
#The symmetric difference operator ^ gets items in either set, but not both. 

first = {1, 2, 3, 4, 5, 6}
second = {4, 5, 6, 7, 8, 9}

print(first | second)
print(first & second)
print(first - second)
print(second - first)
print(first ^ second)

#There can be no duplicates.

##########################################################################################

#Example 5 - Applying the Knowledge in the last Example
a = {1, 2, 10}
b = {0, 2, 4, 5}
print(a & b)


#Rules when decided what to use.

#When to use a dictionary:
#- When you need a logical association between a key:value pair.
#- When you need fast lookup for your data, based on a custom key.
#- When your data is being constantly modified. Remember, dictionaries are mutable.

#When to use the other types:
#- Use lists if you have a collection of data that does not need random access. Try to choose lists when you need a simple, iterable collection that is modified frequently.
#- Use a set if you need uniqueness for the elements.
#- Use tuples when your data cannot change. 
