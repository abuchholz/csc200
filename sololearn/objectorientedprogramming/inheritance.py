class Plane: 
  def __init__(self, name):
    self.name = name

class Airbus(Plane):
  def sound1(self):
    print("VROOM")
        
class Boeing(Plane):
  def sound2(self):
    print("WOOSH")

airplane = Boeing("737-900")
print(airplane.name)
airplane.sound2()


class foo:
  def method(self):
    print("Foo method")
    
class foo1(foo):
  def method1(self):
    print("Foo1 method")
    
class foo2(foo1):
  def method2(self):
    print("Foo2 method")
    
foo = foo2()
foo.method()
foo.method1()
foo.method2()