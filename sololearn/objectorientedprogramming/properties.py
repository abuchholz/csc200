#Set Attribute Error | Commented out to allow code to run
'''
class Airplane:
  def __init__(self, color):
    self.color = color
    
  @property
  def white(self):
    return False

airplane = Airplane(["black", "white"])
print(airplane.white)
airplane.white = True
'''

#Messing Arround with setters
class Airplane:
  def __init__(self, color):
    self.color = color
    self._red_allowed = False

  @property
  def red_allowed(self):
    return self._red_allowed

  @red_allowed.setter
  def red_allowed(self, value):
    if value:
      password = input("Enter the password: ")
      if password == "1234":
        self._red_allowed = value
      else:
        raise ValueError("Alert! Intruder!")

airplane = Airplane(["white", "blue"])
print(airplane.red_allowed)
airplane.red_allowed = True
print(airplane.red_allowed)
