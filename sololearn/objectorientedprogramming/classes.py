class Airplane:
    numberofplanes = 3
    def __init__(self, airline, model):
        self.airline = airline
        self.model = model
    
    def whoosh(self):
        print("WHOOSH")

american = Airplane("American", "A321")
print(american.model)
american.whoosh()

#Calling a Variable in a Class
print(Airplane.numberofplanes)
print(american.numberofplanes)

