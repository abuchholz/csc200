a = 42
print(a)  
b = a  
print(b)
c = [a] 
print(c) 

#Deletes the Object so it no Longer Exists
del a
#Sets b to 100
b = 100  
print(b)
#Sets the first value in the list to -1
c[0] = -1
print(c)  