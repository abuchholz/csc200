import unittest

class Game(object):

    def __init__(self):
        self.rolls = []

    def roll(self, pins):
        self.rolls.append(pins)

    def total_score(self):
        score = 0
        index = 0

        for frame in range(10):
            if self.is_strike(index):
                score += 10 + self.rolls[index+1] + self.rolls[index+2]
                index += 1
            elif self.is_spare(index):
                score += 10 + self.rolls[index+2]
                index += 2
            else:
                score += self.rolls[index] + self.rolls[index+1]
                index += 2
        return score

    def is_spare(self, index):
        return self.rolls[index] + self.rolls[index+1] == 10

    def is_strike(self, index):
        return self.rolls[index] == 10


class GameTest(unittest.TestCase):

    def setUp(self):
        self.game = Game()

    def test_gutter_game(self):
        self.roll_many(0, 20)
        self.assertEqual(0, self.game.total_score())

    def test_all_ones(self):
        self.roll_many(1, 20)
        self.assertEqual(20, self.game.total_score())

    def test_one_spare(self):
        self.game.roll(5)
        self.game.roll(5)
        self.game.roll(3)
        self.roll_many(0, 17)
        self.assertEqual(16, self.game.total_score())

    def test_one_strike(self):
        self.game.roll(10)
        self.game.roll(3)
        self.game.roll(4)
        self.roll_many(0, 16)
        self.assertEqual(24, self.game.total_score())

    def test_perfect_game(self):
        self.roll_many(10, 12)
        self.assertEqual(300, self.game.total_score())

    def roll_many(self, pins, num):
        for i in range(num):
            self.game.roll(pins)
